//
//  Profile.swift
//  testApp3
//
//  Created by Stark on 6/19/18.
//  Copyright © 2018 Justin Demings. All rights reserved.
//

import UIKit

class Profile: UIViewController {
    
    
    
    @IBOutlet weak var fightImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var divisionLabel: UILabel!
    
    @IBOutlet weak var recordLabel: UILabel!
    
    
    var data:Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        fightImage.image = data?.image
        divisionLabel.text = data?.division
        recordLabel.text = data?.record
        nameLabel.text = data?.name
        
        
        //        func setData( data: Data) {
        //            fighterImage.image = data.image
        //            divisionLabel.text = data.division
        //            recordLabel.text = data.record
        //            nameLabel.text = data.name
        //
        
        //    }
        
    }
}
