//
//  BoxingCell.swift
//  testApp3
//
//  Created by Stark on 6/19/18.
//  Copyright © 2018 Justin Demings. All rights reserved.
//

import UIKit

class BoxingCell: UITableViewCell {
    
    
    @IBOutlet weak var boxingImageView: UIImageView!
    
    @IBOutlet weak var boxingTitleLabel: UILabel!
    
    func setData( data: Data) {
        boxingImageView.image = data.image
        boxingTitleLabel.text = data.name
        boxingTitleLabel.text = data.division
        boxingTitleLabel.text = data.record
        
    }
}
