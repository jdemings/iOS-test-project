//
//  appList.swift
//  
//
//  Created by Stark on 6/19/18.
//  Copyright © 2018 Justin Demings. All rights reserved.
//

import UIKit



class appList: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var boxers = [Data]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        let charlo = Data(image:#imageLiteral(resourceName: "charlo"), name: "Jermell Charlo", division: "Super Welterweight", record: "31-0")
        boxers.append(charlo)
        let errol = Data(image:#imageLiteral(resourceName: "errol"),name: "Errol Spence Jr.", division: "Welterweight", record: "24-0")
        boxers.append(errol)
        let lom = Data(image:#imageLiteral(resourceName: "lom") ,name: "Vasyl Lomachenko", division: "Lightweight", record: "11-1")
        boxers.append(lom)
    }
}
extension appList: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return boxers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let boxer = boxers[indexPath.row]
        boxers.append(boxer)
        let cell = tableView.dequeueReusableCell(withIdentifier: "BoxingCell") as! BoxingCell
        
        cell.textLabel?.numberOfLines = 0
        
        cell.setData(data: boxer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let selectedIndex = indexPath.row
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? Profile {
            destination.data = boxers[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
    
}


