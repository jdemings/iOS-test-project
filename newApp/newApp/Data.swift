//
//  Data.swift
//  testApp3
//
//  Created by Stark on 6/19/18.
//  Copyright © 2018 Justin Demings. All rights reserved.
//

import Foundation
import UIKit

class Data {
    
    var name: String
    var division: String
    var record: String
    var image: UIImage
    
    init(image:UIImage, name: String, division: String, record: String) {
        self.name = name
        self.division = division
        self.record = record
        self.image = image
        
    }
}
